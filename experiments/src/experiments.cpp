//
// ofdmframesync_example.c
//
// Example demonstrating the base OFDM frame synchronizer with different
// parameters and options.
//

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <getopt.h>
#include <time.h>
#include <random>
#include <numbers>
#include <iostream>
#include <fstream>

#include <complex>  //Complex needs to be included before liquidsdr
#include <liquid/liquid.h>

#include <HoleOfdmRec.h>
#include <HoleOfdmGen.h>

using namespace std;

class RecComparator: public HoleOfdmRec
{
    const unsigned int *symbols;    //Symbols that should be decoded
    const unsigned int max_symbols; //Number of symbols in symbols
    const ScType* p_tx; //Transmitted hole layout

    int errors = 0;   //How many symbols didn't fit given data
    int fits = 0;  //How many symbols did fit given data
    int frame_counter = 0; //How many frames were received

public:
    /**
     * @brief Receiver comparator, receives one symbol and compares that with given data.
     * @param M
     * @param cp_len
     * @param p
     * @param hole_size
     * @param debug_print
     * @param symbols
     * @param p_tx
     */
    RecComparator(unsigned int M, unsigned int cp_len, const ScType *p, unsigned int hole_size, bool debug_print,
                  const unsigned int *symbols, unsigned int max_symbols, const ScType* p_tx) :
        HoleOfdmRec(M, cp_len, p, hole_size, debug_print),
        symbols(symbols),
        max_symbols(max_symbols),
        p_tx(p_tx)
        {}
    virtual ~RecComparator(){}

    /**
     * @brief How many symbols were correct.
     * @param errors number of not fitting symbols
     * @param fits number of fitting symbols
     */
    void count_errors(int &errors, int &fits)
    {
        errors = this->errors;
        fits = this->fits;
        frame_counter = 0;
        this->errors = 0;
        this->fits = 0;
    }

private:
    /**
     * @brief Callback when symbol is received.
     * @param Y array of M symbols
     * @param p subcarrier allocation array of a current packet (holes applied) [1 x M]
     * @param num_symbols counter of received data symbols
     * @return Ret::Ok to continue receiving, otherwise reset receiver states
     */
    virtual Ret symbolReceived(std::complex<float> *Y, ScType *p_rx, unsigned int num_symbols) override
    {
        //Decode received data subcarriers
        for (unsigned int i = 0; i < M; i++)
        {
            if (frame_counter == 0)
            {
                if (p_rx[i] != p_tx[i])    //Hole layout doesn't fit
                {
                    errors = -1;
                    fits = -1;
                    return Ret::Ercv;
                }
            }

            if (frame_counter*M + i >= max_symbols)  //Check maximal number of symbols
            {
                frame_counter++;
                return Ret::Ercv;
            }

            if (p_rx[i] == ScType::Data)   //Only received data subcarriers
            {
                unsigned int rx_symbol;
                if ((i % 2) == 0)   //BPSK
                {
                    rx_symbol = ((real(Y[i]) < 0) ? 1 : 0);
                }
                else    //QPSK
                {
                    rx_symbol = ((real(Y[i]) < 0) ? 1 : 0) + ((imag(Y[i]) < 0) ? 2 : 0);
                }

                if (rx_symbol != symbols[frame_counter*M + i]) //Symbol doesn't fit
                {
                    errors++;
                }
                else
                {
                    fits++;
                }
            }
        }

        frame_counter++;
        return Ret::Ok;
    }
};

class LiquidComparatorCallback
{
    const unsigned int *symbols;    //Symbols that should be decoded
    const unsigned int max_symbols; //Number of symbols in symbols

    int errors = 0;   //How many symbols didn't fit given data
    int fits = 0;  //How many symbols did fit given data
    int frame_counter = 0; //How many frames were received

public:
    LiquidComparatorCallback(const unsigned int *symbols, unsigned int max_symbols) :
        symbols(symbols), max_symbols(max_symbols)
    {}
    virtual ~LiquidComparatorCallback(){}

    /**
     * @brief How many symbols were correct.
     * @param errors number of not fitting symbols
     * @param fits number of fitting symbols
     */
    void count_errors(int &errors, int &fits)
    {
        errors = this->errors;
        fits = this->fits;
        frame_counter = 0;
        this->errors = 0;
        this->fits = 0;
    }

    // callback function
    //  X          : array of received subcarrier samples [size: M x 1]
    //  p          : subcarrier allocation array [size: M x 1]
    //  M          : number of subcarriers
    //  userdata   : user-defined data pointer
    static int callback(complex<float> *X, unsigned char *p, unsigned int M, void *userdata)
    {
        return static_cast<LiquidComparatorCallback*>(userdata)->symbol_received(X, p, M);
    }

    /**
     * @brief Packet received by liquid receiver.
     * @param X array of received subcarrier samples [size: M x 1]
     * @param p subcarrier allocation array [size: M x 1]
     * @param M number of subcarriers
     * @return zero on success, one to terminate packet
     */
    int symbol_received(complex<float> *X, unsigned char *p, unsigned int M)
    {
        //Decode received data subcarriers
        for (unsigned int i = 0; i < M; i++)
        {
            if (frame_counter * M + i >= max_symbols)  //Check maximal number of symbols
            {
                frame_counter++;
                return 1;
            }

            if (p[i] == OFDMFRAME_SCTYPE_DATA)   //Only data subcarriers
            {
                unsigned int rx_symbol;
                if ((i % 2) == 0)   //BPSK
                {
                    rx_symbol = ((real(X[i]) < 0) ? 1 : 0);
                }
                else    //QPSK
                {
                    rx_symbol = ((real(X[i]) < 0) ? 1 : 0) + ((imag(X[i]) < 0) ? 2 : 0);
                }

                if (rx_symbol != symbols[frame_counter*M + i]) //Symbol doesn't fit
                {
                    errors++;
                }
                else
                {
                    fits++;
                }
            }
        }

        frame_counter++;
        return 0;
    }
};

/**
 * @brief Set of results.
 */
struct ResultSet
{
    int nohole;
    int onehole;
    int holes;
    int liquid;
    ResultSet() :
        nohole(0), onehole(0), holes(0), liquid(0)
    {
    }
    ResultSet(int nohole, int onehole, int holes, int liquid) :
        nohole(nohole), onehole(onehole), holes(holes), liquid(liquid)
    {
    }
};

class Check
{
    HoleOfdmGen *hole_tx;
    RecComparator *hole_rx;

    ofdmframegen liquid_fg;
    LiquidComparatorCallback *liquid_callback;
    ofdmframesync liquid_fs;

    const unsigned int num_symbols; //Number of symbols per packet
    const unsigned int frame_len;   //Length of one frame in samples
    const unsigned int M;   //Number of tones

    unsigned int *symbols;    // symbols
    complex<float> *X;             // channelized symbols

    //Random distributions
    uniform_int_distribution<int> uniform_int;
    uniform_real_distribution<float> uniform_real;
    normal_distribution<float> normal_real;

    const unsigned int cp_len;
    const unsigned int taper_len;
    const unsigned int hole_size;

public:
    /**
     * @brief Create a checker of receivers.
     * @param M number of subcarriers
     * @param cp_len cyclic prefix length
     * @param taper_len length of taper in samples
     * @param hole_size size of hole in subcarriers (only for hole Rx and Tx)
     * @param num_symbols number of symbols to simulate
     */
    Check(unsigned int M, unsigned int cp_len, unsigned int taper_len, unsigned int hole_size, unsigned int num_symbols) :
        num_symbols(num_symbols),
        frame_len(M + cp_len),
        M(M),
        uniform_int(),
        uniform_real(-1, 1),
        normal_real(),
        cp_len(cp_len),
        taper_len(taper_len),
        hole_size(hole_size)
    {
        symbols = new unsigned int[num_symbols*M];
        hole_tx = new HoleOfdmGen[3]
        {
            HoleOfdmGen(M, cp_len, taper_len, nullptr, hole_size),
            HoleOfdmGen(M, cp_len, taper_len, nullptr, hole_size),
            HoleOfdmGen(M, cp_len, taper_len, nullptr, hole_size),
        };
        const HoleOfdmCommon::ScType *p_tx[3];
        for (unsigned int i = 0; i < 3; i++)
        {
            unsigned int temp;
            hole_tx[i].getHoleScType(temp, p_tx[i]);
        }
        hole_rx = new RecComparator[3]
        {
            RecComparator(M, cp_len, nullptr, hole_size, false, symbols, num_symbols * M, p_tx[0]),
            RecComparator(M, cp_len, nullptr, hole_size, false, symbols, num_symbols * M, p_tx[1]),
            RecComparator(M, cp_len, nullptr, hole_size, false, symbols, num_symbols * M, p_tx[2]),
        };
        liquid_callback = new LiquidComparatorCallback(symbols, num_symbols * M),
        X = new complex<float>[M];

        liquid_fg = ofdmframegen_create(M, cp_len, taper_len, nullptr);
        liquid_fs = ofdmframesync_create(M, cp_len, taper_len, nullptr,
                                                       LiquidComparatorCallback::callback,
                                                       static_cast<void*>(liquid_callback));
    }

    virtual ~Check()
    {
        delete[] symbols;
        delete[] hole_tx;
        delete[] hole_rx;
        delete liquid_callback;
        delete[] X;

        // destroy objects
        ofdmframegen_destroy(liquid_fg);
        ofdmframesync_destroy(liquid_fs);
    }

    /**
     * @brief Check one packet for hole receiver and for liquid receiver.
     * @param seed seed for random generator
     * @param snr signal-to-noise ratio [dB]
     * @param errors wrongly received symbols
     * @param fits successfully received symbols
     */
    void one(unsigned int seed, float snr, ResultSet &errors,  ResultSet &fits)
    {
        default_random_engine e1(seed);

        float noise_amp = powf(10.0f, -snr / 20.0f);
        unsigned int dn = ((uniform_real(e1) * 1 + 3) * frame_len);   //Shift of packet in data

        //Reset receivers
        for (unsigned int h = 0; h < 3; h++)
        {
            hole_rx[h].reset();
        }
        ofdmframesync_reset(liquid_fs);

        //----------------------------------------
        // Hole Tx Header
        //----------------------------------------
        complex<float> hole_y[3][8 * frame_len];   // output time series
        unsigned int hole_n[3] = {0};
        unsigned int centers[2] =
        { (unsigned int) (uniform_int(e1) % 102), (unsigned int) (uniform_int(e1) % 102) };

        for (unsigned int h = 0; h < 3; h++)
        {
            if (hole_tx[h].reset(centers, h) != HoleOfdmCommon::Ret::Ok)
            {
                exit(-1);   //Error in hole layout
            }

            //Empty before packet
            while (++hole_n[h] < dn)
            {
                hole_y[h][hole_n[h]] = 0.0;
            }

            // write first S0 symbol
            hole_tx[h].writeS0a(&hole_y[h][hole_n[h]]);
            hole_n[h] += frame_len;

            // write second S0 symbol
            hole_tx[h].writeS0b(&hole_y[h][hole_n[h]]);
            hole_n[h] += frame_len;

            // write S1 symbol
            hole_tx[h].writeS1(&hole_y[h][hole_n[h]]);
            hole_n[h] += frame_len;

            // write second S1 symbol for hole detection
            hole_tx[h].writeHoleS1(&hole_y[h][hole_n[h]]);
            hole_n[h] += frame_len;
        }

        //----------------------------------------
        // Liquid Tx Header
        //----------------------------------------

        ofdmframegen_reset(liquid_fg);
        complex<float> liquid_y[7 * frame_len];   // output time series
        unsigned int liquid_n = 0;
        //Empty before packet
        while (++liquid_n < dn)
        {
            liquid_y[liquid_n] = 0.0;
        }

        // write first S0 symbol
        ofdmframegen_write_S0a(liquid_fg, &liquid_y[liquid_n]);
        liquid_n += frame_len;

        // write second S0 symbol
        ofdmframegen_write_S0b(liquid_fg, &liquid_y[liquid_n]);
        liquid_n += frame_len;

        // write S1 symbol
        ofdmframegen_write_S1(liquid_fg, &liquid_y[liquid_n]);
        liquid_n += frame_len;

        //----------------------------------------
        // Channel and Rx of Header
        //----------------------------------------

        // add channel effects
        for (unsigned int i = 0; i < max(max(hole_n[0], hole_n[1]), max(hole_n[2], liquid_n)); i++)
        {
            complex<float> noise = noise_amp * (normal_real(e1) + normal_real(e1) * 1.0if) * (numbers::sqrt2_v<float> / 2);

            for (unsigned int h = 0; h < 3; h++)
            {
                if (i < hole_n[h])
                {
                    hole_y[h][i] += noise;
                }
            }

            if (i < liquid_n)
            {
                liquid_y[i] += noise;
            }
        }

        // execute receivers on entire header
        hole_rx[0].execute(hole_y[0], hole_n[0]);
        hole_rx[1].execute(hole_y[1], hole_n[1]);
        hole_rx[2].execute(hole_y[2], hole_n[2]);
        ofdmframesync_execute(liquid_fs, liquid_y, liquid_n);

        //----------------------------------------
        // Data Tx and Rx
        //----------------------------------------

        for (unsigned int n = 0; n < num_symbols; n++)
        {
            // load different subcarriers with different data
            for (unsigned int i = 0; i < M; i++)
            {
                if ((i % 2) == 0)   // BPSK
                {
                    symbols[n * M + i] = uniform_int(e1) % 2;
                    X[i] = symbols[n * M + i] ? -1.0f : 1.0f;
                }
                else    // QPSK
                {
                    symbols[n * M + i] = uniform_int(e1) % 4;
                    X[i] = (((symbols[n * M + i] % 2) ? -numbers::sqrt2 : numbers::sqrt2)
                        + (1i * (((symbols[n * M + i] / 2) % 2) ? -numbers::sqrt2 : numbers::sqrt2)));
                }
            }

            // generate OFDM symbol in the time domain
            hole_tx[0].writeSymbol(X, hole_y[0]);
            hole_tx[1].writeSymbol(X, hole_y[1]);
            hole_tx[2].writeSymbol(X, hole_y[2]);
            ofdmframegen_writesymbol(liquid_fg, X, liquid_y);

            // add channel effects
            for (unsigned int i = 0; i < frame_len; i++)
            {
                complex<float> noise = noise_amp * (normal_real(e1) + normal_real(e1) * 1.0if) * (numbers::sqrt2_v<float> / 2);
                hole_y[0][i] += noise;
                hole_y[1][i] += noise;
                hole_y[2][i] += noise;
                liquid_y[i] += noise;
            }

            // execute receivers on one frame
            hole_rx[0].execute(hole_y[0], frame_len);
            hole_rx[1].execute(hole_y[1], frame_len);
            hole_rx[2].execute(hole_y[2], frame_len);
            ofdmframesync_execute(liquid_fs, liquid_y, frame_len);
        }

        //Add empty frames to flush out any partially received frame
        for (unsigned int n = 0; n < 1; n++)
        {
            for (unsigned int i = 0; i < frame_len; i++)
            {
                complex<float> noise = noise_amp * (normal_real(e1) + normal_real(e1) * 1.0if) * (numbers::sqrt2_v<float> / 2);
                hole_y[0][i] = noise;
                hole_y[1][i] = noise;
                hole_y[2][i] = noise;
                liquid_y[i] = noise;
            }
            // execute receivers on last empty frame
            hole_rx[0].execute(hole_y[0], frame_len);
            hole_rx[1].execute(hole_y[1], frame_len);
            hole_rx[2].execute(hole_y[2], frame_len);
            ofdmframesync_execute(liquid_fs, liquid_y, frame_len);
        }

        //----------------------------------------
        // Output
        //----------------------------------------
        hole_rx[0].count_errors(errors.nohole, fits.nohole);
        hole_rx[1].count_errors(errors.onehole, fits.onehole);
        hole_rx[2].count_errors(errors.holes, fits.holes);
        liquid_callback->count_errors(errors.liquid, fits.liquid);
    }

};

/**
 * @param Main function.
 * @param argc
 * @param argv
 * @return 0
 */
int main(int argc, char*argv[])
{
    //Seed with a real random value, if available
    random_device r;

    //Parameters
    const unsigned int M = 256;   //How many carriers
    const unsigned int cp_len = M/8;   // cyclic prefix length
    const unsigned int taper_len = 6*cp_len/8; // taper length
    const unsigned int hole_size = 7; //Size of hole in tones
    const unsigned int symbols_n = 32*8;    //Packet length in symbols

    if (argc < 3)
    {
        cout << "Give runs as a first argument and snr as second!" << endl;
        exit(1);
    }

    //Log to a file
    ostream* output;
    ofstream outputfile;
    if (argc > 3)
    {
        outputfile.open(argv[3]);
        output = &outputfile;
    }
    else
    {
        output = &cout;
    }

    unsigned int runs = atoi(argv[1]);
    float snr = atof(argv[2]);

    Check check(M, cp_len, taper_len, hole_size, symbols_n);

    //Counters
    ResultSet errors;
    ResultSet fits;

    //Repeat
    for (unsigned int i = 0; i < runs; i++)
    {
        check.one(r(), snr, errors, fits);   //Do one transceive
        *output << snr << " "
            << errors.nohole  << " " << fits.nohole  << " "
            << errors.onehole << " " << fits.onehole << " "
            << errors.holes   << " " << fits.holes   << " "
            << errors.liquid  << " " << fits.liquid  << " "
            << endl;  //Print symbol error rate
    }

    if (argc > 3)
    {
        outputfile.close();
    }

    return 0;
}

