#!/bin/bash

for i in {-4..8}
do
    snr=$(bc <<< "scale=3;$i + $1" )
    ./Release/experiments 1000000 $snr "./Release/out_${snr}_file"
    echo "$(date), Done $snr"
done

echo "$(date), End x + $1"

