/**
 * @file Delay.h
 * @brief Small delay.
 * @author Tomas Jakubik
 * @date Feb 24, 2022
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <chrono>

class Delay
{
    std::chrono::time_point<std::chrono::system_clock> t_start; ///< Time of start
    bool active = false;    ///< True if delay is running
    std::chrono::microseconds duration; ///< Delay duration

public:

    Delay(){}
    virtual ~Delay(){}

    /**
     * @brief Time interval from this.
     * @param duration time to wait
     */
    void start(std::chrono::microseconds duration)
    {
        t_start = std::chrono::high_resolution_clock::now();
        active = true;
        this->duration = duration;
    }

    /**
     * @brief Whether delay is being used.
     * @return True if the delay is measuring time.
     */
    const bool is_active()
    {
        return active;
    }

    /**
     * @brief Whether the delay has elapsed.
     * This keeps the delay active.
     * @return true after delay and until elapsed or another start
     */
    const bool ready()
    {
        if (active)
        {
            std::chrono::microseconds dt = std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::high_resolution_clock::now() - t_start);
            if (dt >= duration)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * @brief Know whether some time has elapsed since start.
     * This is true only once.
     * @return true only once when elapsed
     */
    bool elapsed()
    {
        bool ret = ready();
        active = false;
        return ret;
    }
};

#endif /* DELAY_H_ */
