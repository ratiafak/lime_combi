/**
 * @file Timed.h
 * @brief Time certain tasks.
 * @author Tomas Jakubik
 * @date Apr 29, 2021
 */

#ifndef TIMED_H_
#define TIMED_H_

#include <chrono>
#include <climits>
#include <iostream>
#include <iomanip>

class Timed
{
    std::chrono::time_point<std::chrono::system_clock> t_start;
    std::chrono::microseconds sum = std::chrono::microseconds(0);
    std::chrono::microseconds max = std::chrono::microseconds(0);
    std::chrono::microseconds min = std::chrono::microseconds(INT_MAX);

public:
    int cnt = 0;
    float latched_avg = 0;
    int latched_max = 0;
    int latched_min = 0;

    Timed(){}
    virtual ~Timed(){}

    /**
     * @brief Time interval from this.
     */
    void start()
    {
        t_start = std::chrono::high_resolution_clock::now();
    }

    /**
     * @brief Time interval to this.
     */
    void stop()
    {
        std::chrono::microseconds dt = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now() - t_start);
        if (max < dt) max = dt;
        if (min > dt) min = dt;
        sum += dt;
        cnt++;
    }

    /**
     * @brief Store many start() and stop()s for stats to be printed.
     */
    void latch()
    {
        latched_avg = static_cast<float>(sum.count())/cnt;
        latched_max = max.count();
        latched_min = min.count();
        sum = std::chrono::microseconds(0);
        max = std::chrono::microseconds(0);
        min = std::chrono::microseconds(INT_MAX);
        cnt = 0;
    }

    /**
     * @brief Print stats made by last latch().
     */
    void print()
    {
        std::cout << std::setw(10) << latched_avg << " " << std::setw(10) << latched_max << " " << std::setw(10) << latched_min;
    }
};

#endif /* TIMED_H_ */
