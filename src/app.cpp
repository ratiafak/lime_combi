/**
 * @file app.cpp
 * @brief Experimental radio for GMSK and OFDM.
 * @author Tomas Jakubik
 * @date Aug 21, 2020
 */

#include <app.h>
#include <iomanip>
#include <iostream>
#include <fcntl.h>
#include <chrono>
#include <thread>
#include <string>
#include <numbers>
#include <complex>  //Complex needs to be included before liquidsdr
#include <cstring>
#include <pthread.h>
#include <array>
#include <random>
#include <app_videoin.hpp>
#include <app_videoout.hpp>

#include <liquid/liquid.h>

#include <UserFaceEngine.h>
#include <Scope.h>
#include <WrapFFT.h>

using namespace std;
using namespace std::chrono_literals;
using namespace std::chrono;

App::App()
{
    ogen_holes_n = 0;

    for (int i = 0; i < Config::CHANNELS; i++)
    {
        if (Config::GMSK_RX)
        {
            recs[i] = new GmskRec(0x8E666F38, 18, false, i);
        }
    }
    pause_delay = 0;
    if (Config::GMSK_TX)
    {
        gen = new GmskGen(0x8E666F38, 0.01, false, false);
    }

    if (Config::USE_TEMPSCOPE)
    {
        tempscope = new ScopeMarkedFloat(tempscopefile, "tempscope", 1024);
    }
}

App::~App()
{
    for (int i = 0; i < Config::CHANNELS; i++)
    {
        if (Config::GMSK_RX)
        {
            delete recs[i];
        }

    }
    if (Config::GMSK_TX)
    {
        delete gen;
    }

    if (Config::USE_TEMPSCOPE)
    {
        delete tempscope;
    }
}

App app;  ///< Global variables


/**
 * @brief Exit command.
 */
class TxCommand: public UserFaceCommand
{
    const char* name()
    {
        return "tx";
    }

    const char* info()
    {
        return " i - do something debug or Tx related";
    }

    ///@param arg transmitting mode
    void command(const char *arg)
    {
        if (Config::OFDM_TX || Config::GMSK_TX)
        {
            if (arg == nullptr)
            {
                app.mode_enabled = 0;
            }
            else
            {
                stringstream str(arg);
                unsigned int enable;
                str >> enable;
                if (str.fail())    //Off
                {
                    app.mode_enabled = 0;
                }
                else
                {
                    app.mode_enabled = enable;
                }
            }
        }
        else if (Config::USE_TEMPSCOPE)
        {
            if (arg != nullptr)
            {
                app.tempscope->trigger(arg);
            }
            else
            {
                app.tempscope->trigger();
            }
        }
    }
};


/**
 * @brief Loop to handle user and console io.
 */
void console_io_loop()
{
    //Timing for timestamps
    //chrono::system_clock::time_point timestamp_printed = chrono::system_clock::now();
    app.last_receive = system_clock::now();

    system_clock::time_point datarate_last_time = system_clock::now();
    uint32_t datarate_last_bit_count = app.orec.get_bit_count();

    //Mark when to print newline between debug info
    bool mark_line = false;

    //Line of console input
    const constexpr int LINE_SIZE = 128;
    char line[LINE_SIZE];

    //Set console input nonblocking
    fcntl(0, F_SETFL, fcntl(0, F_GETFL) | O_NONBLOCK);

    //Video output
    VideoOut* vidout;
    if (Config::OFDM_RX)
    {
        vidout = new VideoOut;
    }

    while (app.state == App::State::Run)
    {
        //Input
        if (fgets(line, LINE_SIZE, stdin) != nullptr)
        {
            size_t len = strlen(line);
            if (len > 1)
            {
                if (line[len - 1] == '\n')
                {
                    len--;
                }
                lock_guard<mutex> lk(app.cout_mutex);
                UserFaceEngine::in(line, len);
            }
        }

        //Output
        if (Config::PRINT_RADIO_STATS && app.new_stats)
        {
            lock_guard<mutex> lk(app.cout_mutex);

            cout << endl;
            cout << dec;
            cout << setfill(' ');
            cout << "                     avg        max        min" << endl;
            cout << "Radio takes = "; app.radio_op.print(); cout << endl;
            cout << "GRx takes   = "; app.grx_op.print(); cout << endl;
            cout << "GTx takes   = "; app.gtx_op.print(); cout << endl;
            cout << "ORx takes   = "; app.orx_op.print(); cout << endl;
            cout << "OTx takes   = "; app.otx_op.print(); cout << endl;
            cout << "FFT takes   = "; app.fft_op.print(); cout << endl;
            cout << "Sum         = " << app.radio_op.latched_avg + app.grx_op.latched_avg + app.fft_op.latched_avg << " us" << endl;
            cout << "frame_size  = " << Config::FRAME_SIZE << " (" << 1000000.0*Config::FRAME_SIZE/Config::SAMPLE_RATE << " us)" << endl;
            cout << "-----------------         RX         TX" << endl;
            cout << "active          = " << setw(10) << app.rx_status.active          << " " << setw(10) << app.tx_status.active          << endl;
            cout << "droppedPackets  = " << setw(10) << app.rx_status.droppedPackets  << " " << setw(10) << app.tx_status.droppedPackets  << endl;
            cout << "fifoFilledCount = " << setw(10) << app.rx_status.fifoFilledCount << " " << setw(10) << app.tx_status.fifoFilledCount << endl;
            cout << "fifoSize        = " << setw(10) << app.rx_status.fifoSize        << " " << setw(10) << app.rx_status.fifoSize        << endl;
            cout << "linkRate        = " << setw(10) << app.rx_status.linkRate        << " " << setw(10) << app.tx_status.linkRate        << endl;
            cout << "overrun         = " << setw(10) << app.rx_status.overrun         << " " << setw(10) << app.tx_status.overrun         << endl;
            cout << "underrun        = " << setw(10) << app.rx_status.underrun        << " " << setw(10) << app.tx_status.underrun        << endl;
            cout << "OFDM fifo, rx = " << app.rx_fifo.level() << "/" << app.rx_fifo.get_size() <<
                ", tx = " << app.tx_fifo.level() << "/" << app.tx_fifo.get_size() << endl;

            uint32_t datarate_now_bit_count = app.orec.get_bit_count();
            system_clock::time_point datarate_now_time = system_clock::now();
            double bitrate = (datarate_now_bit_count - datarate_last_bit_count)
                    / static_cast<duration<double>>(datarate_now_time - datarate_last_time).count();
            cout << "OFDM bitrate = " << bitrate / (1024 * 1024) << " Mibs";
            cout << ", datarate = " << bitrate / (8 * 1024 * 1024) << " MiBs" << endl;
            datarate_last_time = datarate_now_time;
            datarate_last_bit_count = datarate_now_bit_count;

            app.new_stats = false;
        }

        //Show output video
        if (Config::OFDM_RX)
        {
            while (app.orec.rx_line_fifo.empty() == false)
            {
                vidout->add_line(app.orec.rx_line_fifo.get()->line,
                                 app.orec.rx_line_fifo.get()->line_cnt,
                                 app.orec.rx_line_fifo.get()->frame_cnt);
                app.orec.rx_line_fifo.pop();
            }
            vidout->pool();
        }
        else
        {
            this_thread::sleep_for(chrono::milliseconds(100));  //Wait is replaced by VideoOut::pool()
        }

        //Print scope
        if (Config::USE_TEMPSCOPE)
        {
            lock_guard<mutex> lk(app.cout_mutex);
            app.tempscope->pool_print();
        }

        //Debug info
        {
            lock_guard<mutex> lk(app.cout_mutex);
            App::TimeNumLet timenumlet;
            while (app.debug_fifo.pop(timenumlet))
            {
                if (timenumlet.let == '_')
                {
                    if (mark_line == false)
                    {
                        cout << endl;
                        mark_line = true;
                    }
                }
                else
                {
                    if (mark_line)
                    {
                        printf("@ %7u.%03u s - debug ", timenumlet.time / 1000, timenumlet.time % 1000);
                        mark_line = false;
                    }
                    cout << "'" << timenumlet.let << "'" << (int)timenumlet.num;
                    if(isnan(timenumlet.value) == false)
                    {
                        cout << "(" << timenumlet.value << ")";
                    }
                    cout << " ";
                }
            }
        }

        //Mark when no data are received
        /*//if (((chrono::system_clock::now() - app.last_receive) > 60s)
        //    && ((chrono::system_clock::now() - timestamp_printed) > 60s))
        if (((chrono::system_clock::now() - app.last_receive) > 8s)
            && ((chrono::system_clock::now() - timestamp_printed) > 8s))
        {
            lock_guard<mutex> lk(app.cout_mutex);
            unsigned int now = app.from_start.get_ms();
            timestamp_printed = chrono::system_clock::now();
            printf("@ %7u.%03u s - No data for %7li s\r\n",
                     now / 1000, now % 1000,
                     chrono::duration_cast<chrono::seconds>(chrono::system_clock::now() - app.last_receive).count());
        }*/
    }

    if (Config::OFDM_RX)
    {
        delete vidout;
    }
}



/**
 * @brief Loop to handle hardware, early stages of receiver and transmitter.
 * @param app reference to global variables
 * @param sernum LMS serial number to use
 * @param rx_gain radio Rx gain
 * @param tx_gain radio Tx gain
 */
void transceiver_loop(const char *sernum, float rx_gain, float tx_gain)
{
    //Create and init LimeSDR
    LmsUser lms(Config::SAMPLE_RATE,    //sample_rate
                Config::CENTER_FREQ,    //center_freq
                Config::FRAME_SIZE,     //frame_size
                8*Config::FRAME_SIZE,   //fifo_size
                rx_gain,                //rx_gain
                tx_gain,                //tx_gain
                Config::RX_TX_DELAY);   //rx_tx_delay
    if (lms.init(sernum) == false)
    {
        std::lock_guard<std::mutex> lk(app.app_wake_mutex);
        app.state = App::State::EndRadioError;
        app.app_wake_cv.notify_all();    //Wake app thread because radio ended
        return;
    }

    //Create buffers and FFTs
    Frame empty_in = {0};   ///< Empty frame in case there is nothing to send
    Frame empty_out = {0};  ///< Empty frame in case there is overflow
    int gmsk_in_offset = 0;     ///< How many samples remain from previous frame
    complex<float> time_in[Config::CHANNELS];   ///< Input time data
    complex<float> spec_in[Config::CHANNELS];   ///< Input FFT spectrum
    WrapFFT<float> gmsk_fft(Config::CHANNELS, time_in, spec_in, false);

    //Check that only one Tx is active
    static_assert((Config::GMSK_TX && Config::OFDM_TX) == false,
                  "Two transmitters at once are not supported (Config::GMSK_ENABLE && Config::OFDM_TX)");

    //Stream
    if(lms.stream_start() == false)
    {
        std::lock_guard<std::mutex> lk(app.app_wake_mutex);
        app.state = App::State::EndRadioError;
        app.app_wake_cv.notify_all();    //Wake app thread because radio ended
    }
    while (app.state == App::State::Run)
    {
        //Get frames for Rx and Tx
        Frame *frame_out;
        if ((frame_out = app.tx_fifo.get()) == nullptr) //Also OFDM Tx start
        {
            frame_out = &empty_out;  //Nothing in OFDM fifo
        }
        Frame *frame_in;
        if ((frame_in = app.rx_fifo.emplace()) == nullptr)  //Also OFDM Rx start
        {
            cerr << "OFDM fifo overflow!" << endl;
            frame_in = &empty_in;
        }

        if (Config::GMSK_TX)    //GMSK Tx
        {
            app.gtx_op.start();
            for (int i = 0; i < Config::FRAME_SIZE; i++)
            {
                //Run transmitter
                if (app.gen->get_sample(frame_out->data()[i]))
                {
                    app.pause_delay = 1 + (Config::RX_TX_DELAY / Config::FRAME_SIZE);  //Set delay between Tx and preamble detection on Rx
                }

            }
            if (app.pause_delay > 0)
            {
                app.pause_delay--;
                if (app.pause_delay == 0)   //After a delay
                {
                    for (auto rec : app.recs)
                    {
                        rec->pause();   //Pause all receivers
                    }
                }
            }
            app.gtx_op.stop();
        }

        //Handle radio
        app.radio_op.start();
        if (lms.stream(frame_in->data(), frame_out->data()) == false)
        {
            std::lock_guard<std::mutex> lk(app.app_wake_mutex);
            app.state = App::State::EndRadioError;
            app.app_wake_cv.notify_all();    //Wake app thread because radio ended
            break;
        }
        app.radio_op.stop();

        //OFDM Tx finish
        if (Config::OFDM_TX && (frame_out != &empty_out))
        {
            app.tx_fifo.pop();  //Pop sent data from OFDM Tx fifo
        }

        //GMSK Rx
        if (Config::GMSK_RX)
        {
            for (int frame_pos = 0; frame_pos < (Config::FRAME_SIZE + Config::CHANNELS); frame_pos += Config::CHANNELS)
            {
                //Copy data from frame to time_in
                if (frame_pos < gmsk_in_offset)    //First partial set
                {
                    memcpy(&time_in[gmsk_in_offset], frame_in->data(), sizeof(complex<float>)*(Config::CHANNELS - gmsk_in_offset));
                }
                else if ((frame_pos - gmsk_in_offset + Config::CHANNELS) <= Config::FRAME_SIZE)   //Full sets
                {
                    memcpy(time_in, &frame_in->data()[frame_pos - gmsk_in_offset], sizeof(complex<float>)*Config::CHANNELS);
                }
                else    //Remainder
                {
                    int new_offset = Config::FRAME_SIZE - (frame_pos - gmsk_in_offset);
                    memcpy(time_in, &frame_in->data()[frame_pos - gmsk_in_offset], sizeof(complex<float>)*new_offset);
                    gmsk_in_offset = new_offset;
                    break;  //Will be processed with the next frame
                }

                //Do FFT for GMSK Rx
                app.fft_op.start();
                gmsk_fft.execute();
                app.fft_op.stop();

                //Handle early stages of Rx
                app.grx_op.start();
                for (int i = 0; i < Config::CHANNELS; i++)
                {
                    //Flip spectrum halves to have frequencies fitting channel numbers
                    int flip_spec = i + Config::CHANNELS/2;
                    if (flip_spec >= Config::CHANNELS)
                    {
                        flip_spec -= Config::CHANNELS;
                    }

                    //Run receiver
                    app.recs[i]->put_sample(spec_in[flip_spec]);
                }
                app.grx_op.stop();
            }
        }

        //Incoming power detection for hole placement
        if (Config::OFDM_TX)
        {
            unsigned int holes_n = 0;
            unsigned int holes[2] = {0};
            float ch_pw[2];

            //Do FFT of one frame for OFDM hole placement
            app.fft_op.start();
            memcpy(time_in, &frame_in->data()[Config::FRAME_SIZE - Config::CHANNELS], sizeof(complex<float>)*Config::CHANNELS);
            gmsk_fft.execute();
            app.fft_op.stop();

            //Check each channel for received power
            for (int i = 0; i < Config::CHANNELS; i++)
            {
                float pw = abs(spec_in[i]);  //Linear power on channel i
                if (pw > 2.0) //Threshold for hole
                {
                    if (holes_n < 2)    //Add hole
                    {
                        holes[holes_n] = i;
                        ch_pw[holes_n] = pw;
                        holes_n++;
                    }
                    else    //Overwrite existing hole
                    {
                        if (ch_pw[0] > ch_pw[1])
                        {
                            if (pw > ch_pw[1])
                            {
                                holes[1] = i;
                                ch_pw[1] = pw;
                            }
                        }
                        else
                        {
                            if (pw > ch_pw[0])
                            {
                                holes[0] = i;
                                ch_pw[0] = pw;
                            }
                        }
                    }
                }
            }

            //Store hole info
#if 1 /*Detected or fixed holes*/
            app.ogen_holes_n = holes_n;
            app.ogen_holes[0] = holes[0];
            app.ogen_holes[1] = holes[1];
#else /**/
            app.ogen_holes_n = 1;
            app.ogen_holes[0] = 26 + 32;
            app.ogen_holes[1] = 0;
#endif /**/
        }

        //OFDM Rx finish
        if (Config::OFDM_RX && (frame_out != &empty_in))
        {
            app.rx_fifo.put();  //Put received data to OFDM Rx fifo
        }

        //Wakeup main thread
        {
            std::lock_guard<std::mutex> lk(app.app_wake_mutex);
            app.app_wake_cv.notify_all();    //Wake app thread because new data are available
        }

        //Store stats
        if (Config::PRINT_RADIO_STATS && (app.new_stats == false) && (app.radio_op.cnt >= (Config::SAMPLE_RATE / Config::FRAME_SIZE)))
        {
            lms.status(&app.rx_status, &app.tx_status);
            app.fft_op.latch();
            app.grx_op.latch();
            app.gtx_op.latch();
            app.orx_op.latch();
            app.otx_op.latch();
            app.radio_op.latch();

            app.new_stats = true;
        }

        //Debug mark frames
        app.debug_fifo.put(App::TimeNumLet{0, NAN, 0,'_'});
    }
}


/**
 * @brief Loop to handle later stages of receiver and packet handling.
 * @param sernum LMS serial number to use
 * @param rx_gain radio Rx gain [0 ~ 73 dB]
 * @param tx_gain radio Tx gain [0 ~ 73 dB]
 */
bool app_loop(const char* sernum, float rx_gain, float tx_gain)
{
    //Open log file for scope
    if (Config::USE_TEMPSCOPE)
    {
        app.tempscopefile.open("tempscopefile");
    }

    //Video input
    VideoIn vidin;
    if (Config::OFDM_TX)
    {
        if (vidin.init() == false)
        {
            return false;
        }
    }

    //Start thread for console input and output
    TxCommand tx_command;   //Add command to transmit
    ExitCommand exit_command;  //Add command to exit the app
    thread console_io_thread(console_io_loop);

    //Set priority
    sched_param sch = {sched_get_priority_max(SCHED_FIFO)};
    if (pthread_setschedparam(pthread_self(), SCHED_FIFO, &sch))
    {
        cerr << "Failed to set thread priority: " << strerror(errno) << '\n';
        //Continue with regular priority for debugging
    }

    //Start thread to process transceiver, also with high priority
    thread transceiver_thread(transceiver_loop, sernum, rx_gain, tx_gain);

    while (app.state == App::State::Run)
    {
        //Continue processing GMSK receivers
        if (Config::GMSK_RX)
        {
            for (int ch = 0; ch < Config::CHANNELS; ch++)
            {
                if (app.recs[ch]->process() == GmskRec::State::Done)  //Process the receiver and check for new packet
                {
                    uint8_t rx_packet[18];
                    memcpy(rx_packet, app.recs[ch]->get_data(), sizeof(rx_packet));  //Copy outside
                    app.recs[ch]->resume();   //Reenable receiver

                    //Majority
                    int errors = 0;
                    uint8_t rx_cnt = 0;
                    for (uint8_t bit = 0x1; bit; bit <<= 1)
                    {
                        size_t high_cnt = 0;
                        for (size_t i = 0; i < (sizeof(rx_packet) / 2); i++)
                        {
                            if (rx_packet[i] & bit)
                            {
                                high_cnt++;
                            }
                        }
                        if (high_cnt > (sizeof(rx_packet) / 4))
                        {
                            rx_cnt |= bit;
                            errors += (sizeof(rx_packet) / 2) - high_cnt;
                        }
                        else
                        {
                            errors += high_cnt;
                        }
                    }
                    uint8_t tx_cnt = 0;
                    for (uint8_t bit = 0x1; bit; bit <<= 1)
                    {
                        size_t high_cnt = 0;
                        for (size_t i = (sizeof(rx_packet) / 2); i < sizeof(rx_packet); i++)
                        {
                            if (rx_packet[i] & bit)
                            {
                                high_cnt++;
                            }
                        }
                        if (high_cnt > (sizeof(rx_packet) / 4))
                        {
                            tx_cnt |= bit;
                            errors += (sizeof(rx_packet) / 2) - high_cnt;
                        }
                        else
                        {
                            errors += high_cnt;
                        }
                    }

                    //Reply
                    if (Config::GMSK_TX)
                    {
                        uint8_t tx_packet[9];
                        for (size_t i = 0; i < sizeof(tx_packet); i++)
                        {
                            tx_packet[i] = rx_cnt;
                        }
                        app.gen->set_data(tx_packet, sizeof(tx_packet), (ch - 32) * (2.0f * numbers::pi / 64.0f));
                    }

                    //Print
                    lock_guard<mutex> lk(app.cout_mutex);
                    unsigned int now = app.from_start.get_ms();
                    printf("@ %7u.%03u s - Received ch %02i, rx 0x%02x, tx 0x%02x, errbits %i/%lu\n",
                           now / 1000, now % 1000, ch, rx_cnt, tx_cnt, errors, sizeof(rx_packet)*8);
                }
            }
        }

        //Transmit GMSK packets
        if (Config::GMSK_TX)
        {
            if (app.gen->can_set_data()    //Can send
                && (app.mode_enabled > 0)       //Wants to send
                && (app.recs[26]->is_paused() == false))    //Receiver on the same channel is not paused
            {
                app.gen->set_gain(1.0 / app.mode_enabled);
                app.recs[26]->pause((32 + 8 + 16) * 2 * 8);  //Block receiver on the same channel and make pause after the packet
                app.gen->set_data(reinterpret_cast<const uint8_t*>("Nasrat na hrad! Nasrat na hrad! "), 32,
                                  (26 - 32) * (2.0f * numbers::pi / 64.0f));  //Channel 26
            }
        }

        //OFDM receive
        if (Config::OFDM_RX)
        {
            Frame* orec_frame;
            if ((orec_frame = app.rx_fifo.get()) != nullptr)
            {
                app.orec.execute(orec_frame->data(), Config::FRAME_SIZE);   //Process received frame
                app.rx_fifo.pop();

#if 0 /*Pause GMSK Rx on OFDM Rx*/
                if (app.orec.isFrameOpen()) //Incoming OFDM frame
                {
                    for (auto rec : app.recs)
                    {
                        rec->pause();   //Pause all GMSK receivers
                    }
                }
#endif /*0*/
            }
        }

        //OFDM create packet
        if (Config::OFDM_TX)
        {
            if ((app.ogen.can_send() == true) && (app.ogen_free == 0)   //Previous packet is done
                && (app.tx_fifo.level() < 2)   //Fifo is not filled
                && (app.mode_enabled > 0))  //Tx enabled
            {
                //Compose OFDM message
                uint8_t send_data[Config::MAX_BYTES_PER_TONE][Config::TONES];
                uint16_t line_cnt;
                uint16_t frame_cnt;
                if (vidin.get_line(reinterpret_cast<Line*>(send_data[1]), line_cnt, frame_cnt) == false)  //First line in packet
                {
                    app.state = App::State::EndVideoError;
                    break;
                }
                for (int i = 0; i < Config::TONES; i += 4)
                {
                    send_data[0][i] = line_cnt;  //Put first line number to packet
                    send_data[0][i + 1] = line_cnt >> 8;
                    send_data[0][i + 2] = frame_cnt;  //Put frame number to packet
                    send_data[0][i + 3] = frame_cnt >> 8;
                }
                for (int i = 1 + (Config::LINE_SIZE / Config::TONES); i < Config::MAX_BYTES_PER_TONE;
                    i += (Config::LINE_SIZE / Config::TONES))
                {
                    if (vidin.get_line(reinterpret_cast<Line*>(send_data[i]), line_cnt, frame_cnt) == false)  //More lines in packet
                    {
                        app.state = App::State::EndVideoError;
                        break;
                    }
                }

                //Calculate hole positions
                unsigned int holes_n = 0;
                unsigned int holes[2] = {0};
                if (app.mode_enabled > 1)
                {
                    holes_n = app.ogen_holes_n;
                    holes[0] = app.ogen_holes[0];
                    holes[1] = app.ogen_holes[1];

                    holes[0] *= Config::TONES / Config::CHANNELS;
                    holes[1] *= Config::TONES / Config::CHANNELS;

#if 0 /*Print hole positions*/
                    if (holes_n == 1)
                    {
                        printf("Hole %u\n", app.ogen_holes[0]);
                    }
                    else if (holes_n == 2)
                    {
                        printf("Hole %u, hole %u\n", app.ogen_holes[0], app.ogen_holes[1]);
                    }
#endif /**/
                }

                //Send
                app.ogen.send(send_data, Config::MAX_BYTES_PER_TONE, holes, holes_n);
                app.ogen_free = (3 * (Config::SAMPLE_RATE / Config::FRAME_SIZE) + 500) / 1000;  //Approx 3 ms pause
            }

            //OFDM transmit
            while ((app.tx_fifo.level() < static_cast<int>(app.TX_FIFO_SIZE))   //Fifo is not full
                && ((app.ogen.can_send() == false) || (app.ogen_free > 0))) //Transmitter is transmitting
            {
                Frame* frame = app.tx_fifo.emplace();    //Get frame for output samples
                for (int i = 0; i < Config::FRAME_SIZE; i++)
                {
                    (*frame)[i] = app.ogen.get_sample();  //Fill frame
                }
                app.tx_fifo.put();  //Mark is ready for Tx

                if ((app.ogen.can_send() == true) && (app.ogen_free > 0))   //Packet ended, empty frames remain
                {
                    app.ogen_free--;
                }
            }
        }

        //Wait for new samples
        {
            unique_lock<mutex> lk(app.app_wake_mutex);
            app.app_wake_cv.wait(lk, []
            {
                if (app.state != App::State::Run)   //App needs to end
                {
                    return true;
                }
                if (Config::GMSK_TX)
                {
                    if (app.gen->can_set_data()    //GMSK is free
                        && (app.mode_enabled > 0)       //Want to transmit
                        && (app.recs[26]->is_paused() == false))    //Receiver on that channel is not paused
                    {
                        return true;
                    }
                }
                if (Config::OFDM_TX)
                {
                    if ((app.ogen.can_send() == true) && (app.ogen_free == 0)   //OFDM is free
                        && (app.mode_enabled > 0))      //Want to transmit
                    {
                        return true;
                    }
                    if ((app.tx_fifo.level() < static_cast<int>(app.TX_FIFO_SIZE))  //Tx fifo is not full
                        && ((app.ogen.can_send() == false) || (app.ogen_free > 0)))  //Transmitter is transmitting
                    {
                        return true;
                    }
                }
                if (app.rx_fifo.level() > 0)    //Rx fifo is not empty
                {
                    return true;
                }
                if (Config::GMSK_RX)
                {
                    for (auto rec : app.recs)
                    {
                        if (rec->something_to_process())
                        {
                            return true;
                        }
                    }
                }
                return false;
            });  //Wait for new samples
        }
    }

    //Join with the other threads
    transceiver_thread.join();
    console_io_thread.join();

    //Close log file for scope
    if (Config::USE_TEMPSCOPE)
    {
        app.tempscopefile.close();
    }

    //End with success if user exited
    return app.state == App::State::EndUser;
}
