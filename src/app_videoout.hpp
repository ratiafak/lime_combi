/**
 * @file app_videooht.hpp
 * @brief Show image on screen.
 * @author Tomas Jakubik
 * @date Mar 31, 2022
 */

#ifndef APP_VIDEOOUT_HPP_
#define APP_VIDEOOUT_HPP_

#include <opencv2/opencv.hpp>
#include <global.h>
#include <app_videostore.h>

/**
 * @brief Get lines of data and show resulting video.
 */
class VideoOut
{
    uint16_t line_drawn;    ///< What line was drawn last
    uint16_t frame_drawn;   ///< What frame was shown to the user
    cv::Mat image;          ///< Buffer to compose an image

public:
    VideoOut();
    virtual ~VideoOut();

    /**
     * @brief Add one line to show.
     * @param line buffer of one line of picture data
     * @param line_cnt number of given line
     * @param frame_cnt from this frame
     */
    void add_line(Line& line, uint16_t line_cnt, uint16_t frame_cnt);

    /**
     * @brief Call this periodically to show an image.
     */
    void pool();
};


#endif /* APP_VIDEOOUT_HPP_ */
