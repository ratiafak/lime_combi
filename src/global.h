/**
 * @file global.h
 * @brief Global configuration.
 * @author Tomas Jakubik
 * @date Feb 18, 2022
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_


#define CAMERA 1
#define BASE   2
#ifndef APP_MODE
#   error "APP_MODE was not defined!"
#elif APP_MODE == CAMERA
#   define CAM_MODE     true    ///< Build for camera
#   define FW_PREFIX    "C"
#elif APP_MODE == BASE
#   define CAM_MODE     false   ///< Build for basestation
#   define FW_PREFIX    "B"
#else
#   error "Wrong APP_MODE!"
#endif


#define FW_VER    FW_PREFIX "01.01.0003" //Firmware version of LimeSDR mini combi Lmajor.minor.patch



struct Config
{
    //GMSK
    static constexpr bool GMSK_TX     = !CAM_MODE;  ///< Enable GMSK Rx and Tx
    static constexpr bool GMSK_RX     = !CAM_MODE;  ///< Enable GMSK Rx and Tx
    static constexpr int  CHANNELS    = 64;     ///< Number of channels
    static constexpr int  BAUD_RATE   = 38400;  ///< Baud rate of the GMSK communication [symbols/s]

    //OFDM
    static constexpr bool OFDM_TX     = CAM_MODE;   ///< Enable OFDM transmit
    static constexpr bool OFDM_RX     = !CAM_MODE;  ///< Enable OFDM receive
    static constexpr int  TONES       = 256;        ///< FFT size
    static constexpr int  CP_LEN      = TONES / 8;  ///< Cyclic prefix
    static constexpr int  TAPER_LEN   = 6*Config::CP_LEN/8; ///< Taper between two symbols
    ///@note Max bytes per tone is for QPSK, byte is sent in 4 symbols.
    static constexpr int  MAX_BYTES_PER_TONE = 64;  ///< Maximal length of OFDM packet ~15 ms
    static constexpr int  HOLE_SIZE   = 7;  ///< Size of the hole

    //Common & Hardware
    static constexpr int  FRAME_SIZE  = 1020;   ///< Size of a frame [complex samples] (1020 is an efficient size for LimeSDR)
    static constexpr int  SAMPLE_RATE = (BAUD_RATE * 2) * (CHANNELS); ///< Sample rate [Hz]
    static constexpr int  CENTER_FREQ = 863542400 + (BAUD_RATE * 2) * (CHANNELS / 2);   ///< Center frequency is channel 32 [Hz]
    static constexpr bool PRINT_RADIO_STATS = false;    ///< Whether to print LimeSDR stats each second
    static constexpr bool PRINT_OFDM_STATS = false;     ///< Whether to print OFDM receiver stats
    static constexpr bool USE_TEMPSCOPE = false;        ///< Whether to init and printout tempscope
    static constexpr int  RX_TX_DELAY = (OFDM_TX ? 4 : 8) * FRAME_SIZE; ///< Delay between Rx and Tx, OFDM TX needs lower delay

    //Video
    static constexpr int IMAGE_WIDTH  = TONES * 3;                  ///< Width of the image is a multiple of tones
    static constexpr int IMAGE_HEIGHT = 1080 * IMAGE_WIDTH / 1920;  ///< Keep reasonable aspect ratio
    static constexpr int FRAMERATE    = 60;                         ///< Video framerate
    static constexpr int LINE_SIZE    = IMAGE_WIDTH * 3;            ///< 3 bytes per pixel
};


#endif /* GLOBAL_H_ */
