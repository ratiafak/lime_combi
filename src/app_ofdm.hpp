/**
 * @file app_ofdm.hpp
 * @brief High level OFDM receiver and transmitter.
 * @author Tomas Jakubik
 * @date Apr 1, 2022
 */

#ifndef APP_OFDM_HPP_
#define APP_OFDM_HPP_

#include <HoleOfdmRec.h>
#include <HoleOfdmGen.h>
#include <Fifo.h>
#include <app_videostore.h>

#include <global.h>

struct LinePos
{
    int frame_cnt;  ///< Frame number for this data
    int line_cnt;   ///< Line number for this data
    Line line;      ///< One line of picture data
};

/**
 * @brief OFDM QPSK with holes receiver.
 */
class Receiver: public HoleOfdmRec
{
    static const constexpr size_t LINE_FIFO_SIZE = 256;    ///< Size of the fifo of lines

    msequence ms_data;  ///< Msequence to dewhiten the data
    int frame_cnt;  ///< Frame counter of the packet
    int line_cnt;   ///< Line counter of the first line in the packet

    uint32_t bit_counter;    ///< Increments on each received bit.

public:
    Receiver();
    virtual ~Receiver();

    Fifo<LinePos, LINE_FIFO_SIZE> rx_line_fifo; ///< Buffer for output data

    /**
     * @brief Get cumulative count of received bits.
     * @return bits received since start, overflows at 2^32
     */
    uint32_t get_bit_count() const
    {
        return bit_counter;
    }

private:
    /**
     * @brief Get majority of one word from received data.
     * @param p_rx received tone layout including holes
     * @param row row of received data
     * @param word output majority word
     * @return Ret::Ok on success, otherwise cannot decode and reset Rx
     */
    Receiver::Ret getMajority(const ScType p_rx[Config::TONES], const uint8_t row[Config::TONES], uint32_t &word);

    /**
     * @brief Callback when symbol is received.
     * @param Y array of M symbols
     * @param p subcarrier allocation array of a current packet (holes applied) [1 x M]
     * @param num_symbols counter of received data symbols
     * @return Ret::Ok to continue receiving, otherwise reset receiver states
     */
    virtual Ret symbolReceived(std::complex<float> *Y, ScType *p_rx, unsigned int num_symbols) override;
};

/**
 * @brief OFDM QPSK with holes transmitter.
 */
class Transmitter: public HoleOfdmGen
{
    uint8_t tx_data[Config::MAX_BYTES_PER_TONE][Config::TONES]; ///< Binary data to send
    int tx_byte_counter;    ///< Count what byte in tx_data is being sent
    int tx_bit_counter;     ///< Count what bit in byte is being sent
    int packet_size;        ///< How many bytes are there in each tone of a packet
    msequence ms_data;      ///< Msequence to whiten the data

    Fifo<std::complex<float>, Config::TONES + Config::CP_LEN> sample_fifo;  ///< Fifo for one symbol

    enum class TxStage
    {
        Waiting = 0,    ///< Waiting for a packet
        Start,          ///< Will start Tx on next get_sample()
        S0a,            ///< First S0 symbol and power rise
        S0b,            ///< Second S0 symbol
        S1,             ///< S1 symbol
        S1Hole,         ///< S1 symbol with hole layout information
        Data,           ///< Multiple data symbols
        Tail            ///< Tail, power fall
    };
    TxStage tx_stage;   ///< Packet stage

public:
    Transmitter();
    virtual ~Transmitter();

    /**
     * @brief Get one sample to transmit.
     * @return a sample
     */
    std::complex<float> get_sample();

    /**
     * @brief Put data to be transmitted immediately.
     * @param data data to transmit, bytes for holes, pilots and edges are don't care
     * @param bytes_per_tone how many bytes to send on each tone
     * @param centers centers of up to 2 holes
     * @param centers_n number of holes
     * @return true if Tx starts, false on error
     */
    bool send(const uint8_t data[Config::MAX_BYTES_PER_TONE][Config::TONES],
              unsigned int bytes_per_tone,
              unsigned int *centers = nullptr,
              unsigned int centers_n = 0);

    /**
     * @brief Whether it can send.
     * @return true if can send
     */
    bool can_send()
    {
        return (tx_stage == TxStage::Waiting);  //Can send if in Waiting state
    }

private:
    /**
     * @brief Write current OFDM symbol.
     * This function uses tx_byte_counter and tx_bit_counter.
     * @param y output samples, [size: (M + cp_len) x 1]
     * @return Ret::OK on success
     */
    Ret writeCurrentSymbol(std::complex<float> *y);
};

#endif /* APP_OFDM_HPP_ */
