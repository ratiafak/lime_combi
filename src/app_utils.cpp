/**
 * @file app_utils.cpp
 * @brief Some utilities for user interaction.
 * @author Tomas Jakubik
 * @date Feb 16, 2022
 */

#include <app.h>

using namespace std;
using namespace std::chrono_literals;

/**
 * @brief Time from program start for UserFace.
 * @return time from program start [ms]
 */
uint32_t app_get_time_ms()
{
    return app.from_start.get_ms();
}

/**
 * @brief Debug print info.
 */
void app_debug_print(int number, char letter, float value)
{
    app.debug_fifo.put(App::TimeNumLet{static_cast<uint32_t>(app.from_start.get_ms()), value, static_cast<uint8_t>(number), letter});
}

