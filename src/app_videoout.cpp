/**
 * @file app_videoout.cpp
 * @brief Show image on screen.
 * @author Tomas Jakubik
 * @date Mar 31, 2022
 * @note Needs -l of `opencv_core` and `opencv_highgui`.
 */

#include <global.h>
#include <app_videoout.hpp>
#include <string>
#include <iostream>
#include <app.h>

using namespace std;

/**
 * @brief Allocate image maps.
 */
VideoOut::VideoOut() :
    line_drawn(0),
    frame_drawn(0)
{
    image = cv::Mat::zeros(Config::IMAGE_HEIGHT, Config::IMAGE_WIDTH, CV_8UC3);  //Allocate image buffer
}

/**
 * @brief Clean memory.
 */
VideoOut::~VideoOut()
{
    cv::destroyWindow("Camera");
}

/**
 * @brief Add one line to show.
 * @param line buffer of one line of picture data
 * @param line_cnt number of given line
 * @param frame_cnt from this frame
 */
void VideoOut::add_line(Line& line, uint16_t line_cnt, uint16_t frame_cnt)
{
    if (line_cnt >= Config::IMAGE_HEIGHT)
    {
        return;
    }

    //Clear if there was a break in frames
    if (((frame_cnt - frame_drawn) > 1)
        || ((frame_cnt == frame_drawn) && (line_cnt <= line_drawn)))
    {
        image = cv::Mat::zeros(Config::IMAGE_HEIGHT, Config::IMAGE_WIDTH, CV_8UC3);  //Clear image buffer
    }
    frame_drawn = frame_cnt;

    //Clear lines from last to this
    for (line_drawn = ((line_drawn + 1) % Config::IMAGE_HEIGHT);
        line_drawn != line_cnt;
        line_drawn = ((line_drawn + 1) % Config::IMAGE_HEIGHT))
    {
        VideoStore::clear(image, line_drawn);
    }

    //Add line to buffer
    VideoStore::load(image, line_cnt, line);

    //Clear one next line
    VideoStore::clear(image, (line_cnt + 1) % Config::IMAGE_HEIGHT, 0, 0, 0xff);
}

/**
 * @brief Call this periodically to show an image.
 */
void VideoOut::pool()
{
    cv::imshow("Camera", image);    //Show current image
    if (cv::waitKey(5) == 27)
    {
        app.state = App::State::EndUser;    //End on esc key
    }
}


