/**
 * @file app_utils.h
 * @brief TODO
 * @author Tomas Jakubik
 * @date Feb 18, 2022
 */

#ifndef APP_UTILS_H_
#define APP_UTILS_H_

#include <math.h>

/**
 * @brief Time from program start for UserFace.
 * @return time from program start [ms]
 */
unsigned int app_get_time_ms();

/**
 * @brief Debug print info.
 */
void app_debug_print(int number, char letter, float value = NAN);

#endif /* APP_UTILS_H_ */
