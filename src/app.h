/**
 * @file app.hpp
 * @brief Experimental radio for GMSK and OFDM.
 * @author Tomas Jakubik
 * @date Aug 21, 2020
 */

#ifndef APP_H_
#define APP_H_

#include <app_utils.h>
#include <app_videostore.h>
#include <app_ofdm.hpp>
#include <global.h>

#include <cstdint>
#include <chrono>
#include <mutex>
#include <condition_variable>

#include <LmsUser.h>
#include <GmskGen.h>
#include <GmskRec.h>
#include <Timed.h>
#include <Delay.h>
#include <Fifo.h>
#include <UserFaceEngine.h>
#include <Scope.h>


/**
 * @brief Run until "exit" is input or until error;
 * @param sernum LMS serial number to use
 * @param rx_gain radio Rx gain [0 ~ 73 dB]
 * @param tx_gain radio Tx gain [0 ~ 73 dB]
 * @return true on success
 */
bool app_loop(const char* sernum, float rx_gain, float tx_gain);

/**
 * @brief Receive packet and respond.
 * @param ch received on this channel
 * @param rx_data received data
 * @param len number of bytes in data
 * @return true if Tx was started
 */
bool app_receive_packet(unsigned int ch, const uint8_t* rx_data, uint32_t len);

/**
 * @brief Measure time from program start.
 */
class FromStart
{
    std::chrono::system_clock::time_point start;
public:
    FromStart()
    {
        start = std::chrono::system_clock::now();
    }

    ///@return Get duration from program start.
    auto get_duration()
    {
        return std::chrono::system_clock::now() - start;
    }

    ///@return time from program start in milliseconds
    unsigned int get_ms()
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(get_duration()).count();
    }
};

/**
 * @brief Simplification for one radio frame.
 */
using Frame = std::array<std::complex<float>,Config::FRAME_SIZE>;

/**
 * @brief Global variables and inter-thread communication.
 */
struct App
{
    //Transceivers
    GmskRec* recs[Config::CHANNELS];    ///< Receiver part for all channels
    GmskGen* gen;   ///< Transmitter part for all channels
    unsigned int pause_delay;   ///< Pause receiver when transmitting, this sets delay of Rx and Tx

    Receiver orec;      ///< Receiver for OFDM
    static const constexpr size_t RX_FIFO_SIZE = 16;
    Fifo<Frame, RX_FIFO_SIZE> rx_fifo; ///< Fifo between threads
    Transmitter ogen;   ///< Transmitter for OFDM
    int ogen_free = 0;  ///< Counter of free frames after an OFDM packet
    static const constexpr size_t TX_FIFO_SIZE = 16;
    Fifo<Frame, TX_FIFO_SIZE> tx_fifo; ///< Fifo between threads
    unsigned int ogen_holes_n;  ///< Whether to eliminate 1 or 2 holes
    unsigned int ogen_holes[2]; ///< GMSK channel positions where to put holes

    int mode_enabled = 0;  ///< Whether to Tx continuously

    //Thread sync
    enum class State
    {
        Run = 0,      ///< Run
        EndRadioError,///< EndRadioError
        EndVideoError,///< EndVideoError
        EndUser,      ///< EndUser
    };
    State state = State::Run;   ///< Mark when app needs to end
    std::mutex app_wake_mutex;  ///< Mutex to synchronize new_data_cv
    std::condition_variable app_wake_cv;    ///< Conditional variable to hold app thread until new data

    //Console print
    FromStart from_start;   ///< Measure time from program start
    std::chrono::system_clock::time_point last_receive; ///< Time when last packet was received
    std::mutex cout_mutex;  ///< Mutex to synchronize cout

    //Scopes
    std::ofstream tempscopefile; ///< Where to write logs
    ScopeMarkedFloat* tempscope;

    //Stats
    lms_stream_status_t rx_status, tx_status; ///< Stats to print
    Timed radio_op;     ///< Duration of the radio operation times SAMPLE_RATE/FRAME_SIZE
    Timed gtx_op;       ///< Duration of GMSK Tx operation times SAMPLE_RATE/FRAME_SIZE
    Timed grx_op;       ///< Duration of GMSK Rx operation times SAMPLE_RATE/FRAME_SIZE
    Timed otx_op;       ///< Duration of OFDM Tx operation
    Timed orx_op;       ///< Duration of OFDM Rx operation
    Timed fft_op;       ///< Duration of the fft operation times SAMPLE_RATE/FRAME_SIZE
    bool new_stats = false; ///< Marks new stat values

    App();
    virtual ~App();

    //Debug fifo for information codes
    struct TimeNumLet
    {
        uint32_t time;
        float value;
        uint8_t num;
        char let;
    };
    Fifo<TimeNumLet, 1024> debug_fifo;
};


extern App app;  ///< Global variables


/**
 * @brief Exit command.
 */
class ExitCommand: public UserFaceCommand
{
    const char* name()
    {
        return "exit";
    }

    const char* info()
    {
        return " - End this program";
    }

    ///@param arg ignored
    void command(const char *arg)
    {
        std::lock_guard<std::mutex> lk(app.app_wake_mutex);
        app.state = App::State::EndUser;
        app.app_wake_cv.notify_all();    //Wake app thread because app ends
    }
};


#endif /* APP_H_ */
