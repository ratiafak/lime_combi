/**
 * @file app_gmsk.cpp
 * @brief GMSK part of main application.
 * @author Tomas Jakubik
 * @date Feb 16, 2022
 */

#include <app.h>
#include <packet.h>
#include <UserFaceEngine.h>
#include <cstring>
#include <chrono>
#include <numbers>

using namespace std;
using namespace std::chrono_literals;

/**
 * @brief Print binary data.
 * @param info string printed before the data
 * @param time timestamp printed before info
 * @param data data to print
 * @param len length of data
 */
static void app_print_hex(const char* info, uint32_t time, const uint8_t* data, uint32_t len)
{
    printf("@ %7u.%03u s - %s %u B: ", time / 1000, time % 1000, info, len);
    for(uint32_t i = 0; i < len; i++)
    {
        printf("0x%02x ", data[i]);
    }
    printf("\n");
}

/**
 * @brief Print data from packet from sensor.
 * @param packet packet to print
 * @param len length of packet
 */
static void app_print_data(const gather_t *packet, uint32_t len)
{
    uint16_t timestamp;
    int payload_idx = 0;

    //Print secondly items
    if(packet->secondly > 0)
    {
        timestamp = packet->payload[payload_idx++];
        for (;   //Start after timestamp
            (payload_idx < (packet->secondly + 1))  //Only this many secondly items
                && (len >= (sizeof(gather_t) + sizeof(uint16_t) * ((payload_idx + 1) - GATHER_PAYLOAD_SIZE))); //Only for packet length
            payload_idx++)
        {
            printf("\"Data\", %u, %u, %i.%1u, %u, %i\n",
                     packet->proto.id,
                     timestamp,
                     packet->temperature / 10,
                     (packet->temperature > 0) ? (packet->temperature % 10) : ((-1 * packet->temperature) % 10),
                     packet->humidity,
                     static_cast<int16_t>(packet->payload[payload_idx]));
            timestamp += 1;
        }
    }

    //Print minutely items
    timestamp = packet->payload[payload_idx++];
    for (;   //Start after timestamp
        (len >= (sizeof(gather_t) + sizeof(uint16_t) * ((payload_idx + 1) - GATHER_PAYLOAD_SIZE))); //Only for packet length
        payload_idx++)
    {
        printf("\"Data\", %u, %u, %i.%1u, %u, %i\n",
                 packet->proto.id,
                 timestamp,
                 packet->temperature / 10,
                 (packet->temperature > 0) ? (packet->temperature % 10) : ((-1 * packet->temperature) % 10),
                 packet->humidity,
                 static_cast<int16_t>(packet->payload[payload_idx]));
        timestamp += 60;
    }
}

/**
 * @brief Receive packet and respond.
 * @param ch received on this channel
 * @param rx_data received data
 * @param len number of bytes in data
 * @return true if Tx was started
 */
bool app_receive_packet(unsigned int ch, const uint8_t* rx_data, uint32_t len)
{
    //Map data to packet
    const proto_t* proto = reinterpret_cast<const proto_t*>(rx_data);
    const request_t* request = reinterpret_cast<const request_t*>(rx_data);
    const gather_t* gather = reinterpret_cast<const gather_t*>(rx_data);
    const extra_t* extra = reinterpret_cast<const extra_t*>(rx_data);

    bool txed = false;   //Return true if Tx started

    unsigned int now = app.from_start.get_ms();

    //Ignore packets sent by responder
    if ((proto->header == HEADER_ACK) || (proto->header == HEADER_CONTROL))
    {
        printf("@ %7u.%03u s - Error, own packet received\n", now / 1000, now % 1000);
        return false;
    }

    //Check valid header
    if ((proto->header != HEADER_REQUEST) && (proto->header != HEADER_GATHER) && (proto->header != HEADER_EXTRA))
    {
        app_print_hex("Error, bad header", now, rx_data, len);
        return false;
    }

    //Check length
    if (((proto->header == HEADER_REQUEST) && (len != sizeof(request_t)))
        || ((proto->header == HEADER_GATHER) && ((len < (sizeof(gather_t) - GATHER_PAYLOAD_SIZE * sizeof(uint16_t)))
            || (len > sizeof(gather_t))))
        || ((proto->header == HEADER_EXTRA) && (len != sizeof(extra_t))))
    {
        printf("@ %7u.%03u s - Error, packet with wrong length\n", now / 1000, now % 1000);
        return false;
    }

    //Check sensor id
    if ((proto->id == 0) || (proto->id > UserFaceEngine::Config::CONFIGS_N))
    {
        printf("@ %7u.%03u s - Error, unknown sensor id %u\n",
                 now / 1000, now % 1000,
                 proto->id);
        return false;
    }

    //Get correct channel where the packet was sent
    int32_t received_channel = 0;
    switch (proto->header)
    {
        case HEADER_REQUEST: received_channel = request->channel; break;
        case HEADER_GATHER:  received_channel = gather->channel;  break;
        case HEADER_EXTRA:   received_channel = extra->channel;   break;
    }
    if( received_channel >= Config::CHANNELS)
    {
        printf("@ %7u.%03u s - Error, sensor %u sent on channel %u\n",
                 now / 1000, now % 1000,
                 proto->id, received_channel);
        return false;
    }
    if (received_channel != ch)
    {
        printf("@ %7u.%03u s - Warning, packet received on %u instead of %u\n",
                 now / 1000, now % 1000,
                 ch, received_channel);
    }

    //Get the right sensor
    UserFaceEngine::Config::SensorControl *sensor = &UserFaceEngine::user_config.sensor_controls[proto->id - 1];

    //Respond
    if ((proto->header == HEADER_REQUEST)  //Sensor requested control
        || sensor->changed)  //Config changed
    {
        sensor->config.proto.seq = proto->seq;  //Respond with the same seq
        if (app.gen->set_data(reinterpret_cast<uint8_t*>(&sensor->config), sizeof(control_t),
                              (received_channel - 32) * (2.0f * numbers::pi / 64.0f)))
        {
            txed = true;

            if (proto->header == HEADER_REQUEST)  //Sensor requested config
            {
                char fw_ver[17];
                strncpy(fw_ver, request->fw_ver, 16);   //Get safe FW_VER string
                fw_ver[16] = '\0';
                printf("@ %7u.%03u s - Sensor %u restarted on ch %u, fw=\"%s\"\n", now / 1000,
                         now % 1000, proto->id, received_channel, fw_ver);
            }
            else
            {
                printf("@ %7u.%03u s - Packet from %u on ch %u, config sent\n", now / 1000,
                         now % 1000, proto->id, received_channel);
            }
        }
        else
        {
            if (proto->header == HEADER_REQUEST)  //Sensor requested config
            {
                char fw_ver[17];
                strncpy(fw_ver, request->fw_ver, 16);   //Get safe FW_VER string
                fw_ver[16] = '\0';
                printf("@ %7u.%03u s - Sensor %u restarted on ch %u, fw=\"%s\"\n", now / 1000,
                         now % 1000, proto->id, received_channel, fw_ver);
            }
            printf("@ %7u.%03u s - Error, config not sent to %u on ch %u because of ongoing Tx\n",
                     now / 1000, now % 1000, proto->id, received_channel);
        }
    }
    else    //Regular ACK is to be sent
    {
        ack_t ack = { .proto = { .header = HEADER_ACK,
                                 .id = proto->id,
                                 .seq = proto->seq } };   //Respond with the same seq and id

        //Send
        if (app.gen->set_data(reinterpret_cast<uint8_t*>(&ack), sizeof(ack_t),
                              (received_channel - 32) * (2.0f * numbers::pi / 64.0f)))
        {
            txed = true;

            printf("@ %7u.%03u s - Packet from %u on ch %u, ack sent\n",
                     now / 1000, now % 1000, proto->id, received_channel);
        }
        else
        {
            printf("@ %7u.%03u s - Error, packet from %u on ch %u not acked because of ongoing Tx\n",
                     now / 1000, now % 1000, proto->id, received_channel);
        }
    }

    //Check seq
    if (proto->header == HEADER_REQUEST)
    {
        sensor->seq = proto->seq;   //Store seq
        app.last_receive = chrono::system_clock::now();   //Mark successful communication
    }
    else if (proto->seq == ((sensor->seq + 1) & 0x3))  //One more would be ok, other is bad sequence number
    {
        sensor->seq = proto->seq;   //Store seq
        app.last_receive = chrono::system_clock::now();   //Mark successful communication

        //Print changes in configuration
        if ((sensor->sent == true) && (sensor->changed == false))
        {
            sensor->sent = false;
            printf("@ %7u.%03u s - Config applied in %u\n",
                     now / 1000, now % 1000, proto->id);
        }

        //Debug trigger scopes for missing packets
        if (proto->header == HEADER_GATHER)
        {
            for(int i = gather->secondly; i > 0; i--)
            {
                if (static_cast<int16_t>(gather->payload[i]) != -1)
                {
                    printf("@ %7u.%03u s - Missed packet on %u\n",
                             now / 1000, now % 1000, gather->payload[i]);
                    break;
                }
            }
        }
    }
    else if (proto->seq == sensor->seq)   //Repeated packet
    {
        printf("@ %7u.%03u s - Repeated packet from %u\n",
                 now / 1000, now % 1000, proto->id);
    }
    else    //Completely wrong sequence
    {
        printf("@ %7u.%03u s - Error, bad sequence from %u, possibly restart\n",
                 now / 1000, now % 1000, proto->id);
    }

    //Print packet data
    if (proto->header == HEADER_GATHER)
    {
        app_print_data(gather, len);
    }
    else if (proto->header == HEADER_EXTRA)
    {
        printf("@ %7u.%03u s - Extra from %u, nearest timestamp %u, extra seq %u, value %i\n",
                 now / 1000, now % 1000,
                 proto->id,
                 extra->nearest_timestamp,
                 extra->extra_seq,
                 static_cast<int16_t>(extra->lmp));
    }

    //Mark sent config
    if ((proto->header == HEADER_REQUEST) || sensor->changed)
    {
        sensor->changed = false;
        sensor->sent = true;
    }

    return txed;
}

