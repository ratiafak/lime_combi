/**
 * @file app_videostore.h
 * @brief Store and load one line of image to byte array.
 * @author Tomas Jakubik
 * @date Apr 1, 2022
 */

#ifndef APP_VIDEOSTORE_H_
#define APP_VIDEOSTORE_H_

#include <opencv2/opencv.hpp>
#include <global.h>

/**
 * @brief Simplification for one line of image data.
 */
struct Line
{
    uint8_t data[Config::LINE_SIZE / Config::TONES][Config::TONES];    ///< RGB data of one line of image
};

/**
 * @brief Store and load one line
 */
namespace VideoStore
{
    /**
     * @brief Store one line from image to data.
     * @param image from this image
     * @param at this line
     * @param data to this data
     */
    void store(cv::Mat &image, int line_cnt, Line &data);

    /**
     * @brief Load one line from data to image.
     * @param image to this image
     * @param at this line
     * @param data from this data
     */
    void load(cv::Mat &image, int line_cnt, Line &data);

    /**
     * @brief Make one line of image black.
     * @param image to this image
     * @param line_cnt this line
     * @param b blue
     * @param g green
     * @param r red
     */
    void clear(cv::Mat &image, int line_cnt, uint8_t b = 0, uint8_t g = 0, uint8_t r = 0);
};

#endif /* APP_VIDEOSTORE_H_ */
