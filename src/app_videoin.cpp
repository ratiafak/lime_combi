/**
 * @file app_cam.cpp
 * @brief Camera access.
 * @author Tomas Jakubik
 * @date Mar 31, 2022
 *
 * @note Needs -l of `opencv_core` and `opencv_videoio`.
 */

#include <global.h>
#include <app_videoin.hpp>
#include <iostream>
#include <string>

using namespace std;

/**
 * @brief Get gstreamer source from raspberry camera.
 * @return pipeline string
 */
string VideoIn::gstreamer_pipeline()
{
   /* return " libcamerasrc ! video/x-raw, "
        " width=(int)" + to_string(Config::IMAGE_WIDTH) + ","
        " height=(int)" + to_string(Config::IMAGE_HEIGHT) + ","
        " framerate=(fraction)" + to_string(Config::FRAMERATE) + "/1 !"
        " videoconvert !"
        " video/x-raw !"
        " appsink";*/
    return " libcamerasrc ! video/x-raw, "
        " width=(int)" + to_string(Config::IMAGE_WIDTH) + ","
        " height=(int)" + to_string(Config::IMAGE_HEIGHT) + " !"
        " videoconvert !"
        " video/x-raw !"
        " appsink";
}

/**
 * @brief Get one line of image data.
 * @param line where to store the data
 * @param line_out line number
 * @param frame_out frame number
 * @return true on success
 */
bool VideoIn::get_line(Line* line, uint16_t &line_out, uint16_t &frame_out)
{
    if (inited == false)    //Camera was not initialized
    {
#ifdef DEBUG /*Fake video in*/
        return true;
#else /*Fake video in*/
        return false;
#endif /*Fake video in*/
    }

    //First line, need to get image from camera
    if (line_cnt == 0)
    {
        if (cap->retrieve(image) == false)
        {
            cerr << "Capture read error" << endl;
            return false;
        }
        frame_cnt++;
    }

    //Copy line to output buffer
    VideoStore::store(image, line_cnt, *line);
    line_out = line_cnt;
    frame_out = frame_cnt;

    //Increment line counter
    if (++line_cnt >= Config::IMAGE_HEIGHT)
    {
        line_cnt = 0;
    }

    //Periodically grab to keep framebuffer low
    if (line_cnt % (Config::IMAGE_HEIGHT / 16) == 0)
    {
        cap->grab();
    }

    return true;
}

/**
 * @brief Initialize camera input.
 * @return
 */
bool VideoIn::init()
{
    if (inited)  //Already initialized
    {
        return false;
    }

    //Get string for gstreamer to get video at requested resolution
    string pipeline = gstreamer_pipeline();
    cout << "Using pipeline: \n\t" << pipeline << "\n\n\n";

    //Open capture device
    cap = new cv::VideoCapture(pipeline, cv::CAP_GSTREAMER);
    inited = true;
    if (cap->isOpened() == false)
    {
        cerr << "Failed to open camera." << endl;
        deinit();
#ifdef DEBUG /*Fake video in*/
        return true;
#else /*Fake video in*/
        return false;
#endif /*Fake video in*/
    }

    //Get test image
    if (cap->read(image) == false)
    {
        cout << "Capture read error" << endl;
        deinit();
        return false;
    }

    //Check that there are 3 bytes for pixel
    int elemsize = 0;
    if ((elemsize = image.elemSize()) != 3)
    {
        cout << "There are " << elemsize << " (not 3) bytes per pixel" << endl;
        deinit();
        return false;
    }

    line_cnt = 0;   //Start at first line
    return true;
}

/**
 * @brief Deinitialize camera input.
 */
void VideoIn::deinit()
{
    if (inited)
    {
        cap->release();
        delete cap;
    }
}



