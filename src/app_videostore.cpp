/**
 * @file app_videostore.cpp
 * @brief Store and load one line of image to byte array.
 * @author Tomas Jakubik
 * @date Apr 1, 2022
 */

#include <app_videostore.h>


/**
 * @brief Store one line from image to data.
 * @param image from this image
 * @param at this line
 * @param data to this data
 */
void VideoStore::store(cv::Mat &image, int line_cnt, Line &data)
{
    for (int tone = 0; tone < Config::TONES; tone++)    //Width of the OFDM
    {
        for (int pix = 0; pix < (Config::IMAGE_WIDTH / Config::TONES); pix++)   //Needs n pixels for one tone
        {
            cv::Vec3b pixel = image.at<cv::Vec3b>(line_cnt, (Config::IMAGE_WIDTH / Config::TONES) * tone + pix);   //Get one pixel
            data.data[3 * pix][tone] = pixel.val[0];
            data.data[3 * pix + 1][tone] = pixel.val[1];
            data.data[3 * pix + 2][tone] = pixel.val[2];
        }
    }
}

/**
 * @brief Load one line from data to image.
 * @param image to this image
 * @param at this line
 * @param data from this data
 */
void VideoStore::load(cv::Mat &image, int line_cnt, Line &data)
{
    for (int tone = 0; tone < Config::TONES; tone++)    //Width of the OFDM
    {
        for (int pix = 0; pix < (Config::IMAGE_WIDTH / Config::TONES); pix++)   //Needs n pixels for one tone
        {
            cv::Vec3b pixel(data.data[3 * pix    ][tone],
                            data.data[3 * pix + 1][tone],
                            data.data[3 * pix + 2][tone]);  //Create a pixel
            image.at<cv::Vec3b>(line_cnt, (Config::IMAGE_WIDTH / Config::TONES) * tone + pix) = pixel;   //Put pixel to image
        }
    }
}

/**
 * @brief Make one line of image black.
 * @param image to this image
 * @param line_cnt this line
 * @param b blue
 * @param g green
 * @param r red
 */
void VideoStore::clear(cv::Mat &image, int line_cnt, uint8_t b, uint8_t g, uint8_t r)
{
    for (int pix = 0; pix < Config::IMAGE_WIDTH; pix++)   //All pixels in a row
    {
        cv::Vec3b pixel(b, g, r);  //Create a black pixel
        image.at<cv::Vec3b>(line_cnt, pix) = pixel;   //Put pixel to image
    }
}
