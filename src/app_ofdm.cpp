/**
 * @file app_ofdm.cpp
 * @brief High level OFDM receiver and transmitter.
 * @author Tomas Jakubik
 * @date Feb 16, 2022
 */

#include <app_ofdm.hpp>
#include <cstring>
#include <numbers>
#include <iostream>

using namespace std;

/**
 * @brief OFDM receiver constructor.
 */
Receiver::Receiver() :
    HoleOfdmRec(Config::TONES, Config::CP_LEN, nullptr, Config::HOLE_SIZE, Config::PRINT_OFDM_STATS),
    frame_cnt(0),
    line_cnt(0),
    bit_counter(0)
{
    ms_data = msequence_create_default(15);
}

/**
 * @brief OFDM receiver destructor.
 */
Receiver::~Receiver()
{
    msequence_destroy(ms_data);
}

/**
 * @brief Get majority of one word from received data.
 * @param p_rx received tone layout including holes
 * @param row row of received data
 * @param word output majority word
 * @return Ret::Ok on success, otherwise cannot decode and reset Rx
 */
Receiver::Ret Receiver::getMajority(const ScType p_rx[Config::TONES], const uint8_t row[Config::TONES], uint32_t &word)
{
    //Count bits
    int ones[32] = {0};
    int zeros[32] = {0};
    for (int i = 0; i < Config::TONES; i++)
    {
        if (p_rx[i] == ScType::Data)
        {
            for (int bit = 0; bit < 8; bit++)
            {
                if (row[i] & (1 << bit))
                {
                    ones[(i % 4) * 8 + bit]++;
                }
                else
                {
                    zeros[(i % 4) * 8 + bit]++;
                }
            }
        }
    }

    //Get majority
    word = 0;
    for (int i = 0; i < 32; i++)
    {
        if (ones[i] > (zeros[i] + 1))
        {
            word |= (1 << i);
        }
        else if (zeros[i] > (ones[i] + 1))
        {
            //0 bit
        }
        else
        {
            return Ret::Ercv;   //No count is bigger at least by 2
        }
    }

    return Ret::Ok;
}

/**
 * @brief Callback when symbol is received.
 * @param Y array of M symbols
 * @param p subcarrier allocation array of a current packet (holes applied) [1 x M]
 * @param num_symbols counter of received data symbols
 * @return Ret::Ok to continue receiving, otherwise reset receiver states
 */
Receiver::Ret Receiver::symbolReceived(complex<float> *Y, ScType *p_rx, unsigned int num_symbols)
{
    if (num_symbols == 0)
    {
        msequence_reset(ms_data);   //Reset whitening sequence on first symbol
    }

    //Get buffer for received data
    LinePos* rx_data = rx_line_fifo.emplace();
    if (rx_data == nullptr)
    {
        cerr << "Memory bug" << endl;
        return Ret::Eimem;
    }

    //Get position in a packet
    int rx_byte_counter = num_symbols / 4;  //Byte of packet offset
    int rx_bit_counter = 2 * (num_symbols % 4); //Bin in byte offset
    int buffer_ofset = 0;   //Buffer offset, first byte is frame and line info
    int line_cnt_offset = 0;    //Line position of current byte inside packet
    if (rx_byte_counter > 0)
    {
        buffer_ofset = (rx_byte_counter - 1) % (Config::LINE_SIZE / Config::TONES); //Buffer offset for a line of image data
        line_cnt_offset = (rx_byte_counter - 1) / (Config::LINE_SIZE / Config::TONES); //Count lines in packet
    }

    //Decode received data subcarriers
    for (int i = 0; i < Config::TONES; i++)
    {
        int tone = (i + (Config::TONES / 2)) % Config::TONES;   //Flip tones to have lowest frequency at 0

        unsigned int dewhiter = msequence_advance(ms_data);   //Run dewhitening msequence for all tones as in Tx
        dewhiter ^= msequence_advance(ms_data) << 1;

        if (p_rx[tone] == ScType::Data)   //Only received data subcarriers
        {
            unsigned int rx_symbol;
            rx_symbol = ((real(Y[tone]) < 0) ? 0x1 : 0) + ((imag(Y[tone]) < 0) ? 0x2 : 0);    //Decode QPSK
            rx_symbol ^= dewhiter;  //Dewhiten

            if (rx_bit_counter == 0)
            {
                rx_data->line.data[buffer_ofset][i] = rx_symbol;
            }
            else
            {
                rx_data->line.data[buffer_ofset][i] |= rx_symbol << rx_bit_counter;
            }

            bit_counter += 2;    //Cumulative bit counter
        }
        else if (p_rx[tone] == ScType::Pilot)   //Pilots with different data
        {
            rx_data->line.data[buffer_ofset][i] = 0xd0;
        }
        else
        {
            rx_data->line.data[buffer_ofset][i] = 0x30;
        }
    }

    //First byte received, get info about frame and line position
    if ((rx_byte_counter == 0) && (rx_bit_counter == 6))
    {
        uint32_t word = 0;
        if (getMajority(p_rx, rx_data->line.data[0], word) != Ret::Ok)
        {
            return Ret::Ercv;
        }

        //Get average byte value and compose line and frame numbers
        line_cnt = word & 0xffff;
        frame_cnt = word >> 16;

        if (debug_print)
        {
            static int last_time = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
            int this_time = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
            cout << this_time - last_time << "Frame " << frame_cnt << endl;
            last_time = this_time;
        }
    }

    //Line received, put to fifo
    if ((buffer_ofset == (Config::LINE_SIZE / Config::TONES - 1)) && (rx_bit_counter == 6))
    {
        if ((line_cnt + line_cnt_offset) >= Config::IMAGE_HEIGHT)
        {
            frame_cnt = (frame_cnt + 1) & 0xffff;
        }
        rx_data->line_cnt = (line_cnt + line_cnt_offset) % Config::IMAGE_HEIGHT; //Line in an image of current data
        rx_data->frame_cnt = frame_cnt;
        rx_line_fifo.put();

        if (debug_print)
        {
            cout << "Line " << line_cnt + line_cnt_offset << endl;
        }
    }

    //End after full packet
    if (((rx_byte_counter == (Config::MAX_BYTES_PER_TONE - 1)) && (rx_bit_counter == 6))    //Last symbol
        || (rx_byte_counter >= Config::MAX_BYTES_PER_TONE))
    {
        return Ret::Edone;
    }
    else
    {
        return Ret::Ok;
    }
}

/**
 * @brief Construct transmitter.
 */
Transmitter::Transmitter() :
    HoleOfdmGen(Config::TONES, Config::CP_LEN, Config::TAPER_LEN, nullptr, Config::HOLE_SIZE, 1.0 / 7),
    tx_byte_counter(0),
    tx_bit_counter(0),
    packet_size(0),
    tx_stage(TxStage::Waiting)
{
    ms_data = msequence_create_default(15);
}

/**
 * @brief Destroy transmitter.
 */
Transmitter::~Transmitter()
{
    msequence_destroy(ms_data);
}

/**
 * @brief Write current OFDM symbol.
 * This function uses tx_byte_counter and tx_bit_counter.
 * @param y output samples, [size: (M + cp_len) x 1]
 * @return Ret::OK on success
 */
Transmitter::Ret Transmitter::writeCurrentSymbol(complex<float> *y)
{
    complex<float> X[Config::TONES];

    //Prepare symbol for all tones
    for (int i = 0; i < Config::TONES; i++)
    {
        int tone = (i + (Config::TONES / 2)) % Config::TONES;   //Flip tones to have lowest frequency at 0

        uint8_t symbol = (tx_data[tx_byte_counter][i] >> tx_bit_counter) & 0x3; //Get symbol
        symbol ^= msequence_advance(ms_data);   //Whiten by an msequence
        symbol ^= msequence_advance(ms_data) << 1;

        X[tone] = (((symbol & 0x1) ? -numbers::sqrt2 : numbers::sqrt2)
            + (1i * ((symbol & 0x2) ? -numbers::sqrt2 : numbers::sqrt2)));  //QPSK
    }

    //Move to next symbol
    tx_bit_counter += 2;
    if (tx_bit_counter >= 8)
    {
        tx_bit_counter = 0;
        tx_byte_counter++;
    }

    //Generate symbol
    return writeSymbol(X, y);
}

/**
 * @brief Get one sample to transmit.
 * @return a sample
 */
complex<float> Transmitter::get_sample()
{
    if (tx_stage == TxStage::Waiting)   //Nothing to send
    {
        return 0;
    }

    //Nothing in fifo, prepare next symbol
    if (sample_fifo.empty())
    {
        complex<float> *symbol = sample_fifo.get_fill();   //One symbol directly written to fifo
        switch (tx_stage)
        {
            case TxStage::Start:
                tx_stage = TxStage::S0a;
                writeS0a(symbol);
                break;

            case TxStage::S0a:
                tx_stage = TxStage::S0b;
                writeS0b(symbol);
                break;

            case TxStage::S0b:
                tx_stage = TxStage::S1;
                writeS1(symbol);
                break;

            case TxStage::S1:
                tx_stage = TxStage::S1Hole;
                writeHoleS1(symbol);
                break;

            case TxStage::S1Hole:
                tx_stage = TxStage::Data;
                tx_byte_counter = 0;
                tx_bit_counter = 0;
                [[fallthrough]];

            case TxStage::Data:
                if (tx_byte_counter < packet_size)  //Still something to send
                {
                    writeCurrentSymbol(symbol);
                }
                else    //Packet ended
                {
                    tx_stage = TxStage::Tail;
                    writeTail(symbol);
                }
                break;

            default:
                //Done transmitting
                tx_stage = TxStage::Waiting;
                return 0;
        }

        sample_fifo.filled(Config::TONES + Config::CP_LEN);
    }

    //Return one sample from fifo
    std::complex<float> sample;
    if (sample_fifo.pop(sample))
    {
        return sample;
    }

    return 0;
}

/**
 * @brief Put data to be transmitted immediately.
 * @param data data to transmit, bytes for holes, pilots and edges are don't care
 * @param bytes_per_tone how many bytes to send on each tone
 * @param centers centers of up to 2 holes
 * @param centers_n number of holes
 * @return true if Tx starts, false on error
 */
bool Transmitter::send(const uint8_t data[Config::MAX_BYTES_PER_TONE][Config::TONES],
                       unsigned int bytes_per_tone,
                       unsigned int *centers,
                       unsigned int centers_n)
{
    if ((tx_stage != TxStage::Waiting)   //Already transmitting
        || (bytes_per_tone > Config::MAX_BYTES_PER_TONE)) //Too long
    {
        return false;
    }

    //Reset whitening sequence
    msequence_reset(ms_data);

    //Copy data to internal buffer
    memcpy(tx_data, data, (sizeof(uint8_t)) * Config::TONES * bytes_per_tone);
    packet_size = bytes_per_tone;

    //Start transmitting
    if (reset(centers, centers_n) != Ret::Ok)
    {
        return false;
    }
    tx_stage = TxStage::Start;
    return true;
}

