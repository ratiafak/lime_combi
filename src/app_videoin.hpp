/**
 * @file app_videoin.hpp
 * @brief Camera access.
 * @author Tomas Jakubik
 * @date Mar 31, 2022
 */

#ifndef APP_VIDEOIN_HPP_
#define APP_VIDEOIN_HPP_

#include <opencv2/opencv.hpp>
#include <string>
#include <app_videostore.h>

/**
 * @brief Capture video and give lines of data.
 */
class VideoIn
{
    bool inited;            ///< True after initialization
    cv::VideoCapture* cap;  ///< Capture device
    cv::Mat image;          ///< Buffer for one image
    int line_cnt;           ///< Line of image to give out
    int frame_cnt;          ///< Frame to give out

    /**
     * @brief Get gstreamer source from raspberry camera.
     * @return pipeline string
     */
    std::string gstreamer_pipeline();

public:
    VideoIn() :
        inited(false), cap(nullptr), line_cnt(0), frame_cnt(0)
    {
    }

    virtual ~VideoIn()
    {
        deinit();
    }

    /**
     * @brief Get one line of image data.
     * @param line where to store the data
     * @param line_out line number
     * @param frame_out frame number
     * @return true on success
     */
    bool get_line(Line* line, uint16_t &line_out, uint16_t &frame_out);

    /**
     * @brief Initialize camera input.
     * @return
     */
    bool init();

    /**
     * @brief Deinitialize camera input.
     */
    void deinit();
};


#endif /* APP_VIDEOIN_HPP_ */
