# lime_combi

## Combine GMSK and OFDM transceivers

- Built for Debian stable and crosscompiled for 64 bit Raspberry
- Using Eclipse CDT to manage the project
- Using LimeSuite LMS API library
- Using liquidSDR library
- Using FFTW3 library

## Demo
[This demo](https://youtu.be/M0JLoLhRnvo) shows how it works together with a [gas_sensor](https://gitlab.com/ratiafak/gas_sensor).


